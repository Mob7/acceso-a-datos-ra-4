package Base;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by ASIR on 03/03/2016.
 */
public class Canciones {

    public static String Coleccion = "Canciones";

    private ObjectId id;
    private String titulo;
    private float precioItunes;
    private int numeroPista;
    private Date fechaSalida;
    private String artista;

    public static String getColeccion() {
        return Coleccion;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getPrecioItunes() {
        return precioItunes;
    }

    public void setPrecioItunes(float precioItunes) {
        this.precioItunes = precioItunes;
    }

    public int getNumeroPista() {
        return numeroPista;
    }

    public void setNumeroPista(int numeroPista) {
        this.numeroPista = numeroPista;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String toString(){
        return titulo;
    }
}
