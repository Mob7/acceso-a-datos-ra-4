package Base;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by ASIR on 03/03/2016.
 */
public class Discos {

    public static String Coleccion = "Discos";

    private ObjectId id;
    private String titulo;
    private Float precioCompra;
    private int copiasVendidas;
    private Date fechaSalida;
    private String artista;

    public static String getColeccion() {
        return Coleccion;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(Float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getCopiasVendidas() {
        return copiasVendidas;
    }

    public void setCopiasVendidas(int copiasVendidas) {
        this.copiasVendidas = copiasVendidas;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public String toString(){
        return titulo;
    }
}
