package Base;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * Created by ASIR on 03/03/2016.
 */
public class Artistas {

    public static String Coleccion = "Artistas";

    private ObjectId id;
    private String nombre;
    private int telefono;
    private float precioActuacion;
    private Date fechaNacimiento;

    public static String getColeccion() {
        return Coleccion;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public float getPrecioActuacion() {
        return precioActuacion;
    }

    public void setPrecioActuacion(float precioActuacion) {
        this.precioActuacion = precioActuacion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String toString(){
        return nombre;
    }
}
