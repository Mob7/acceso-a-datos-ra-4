package Gui;

import Base.Artistas;
import Base.Canciones;
import Base.Discos;
import Util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sergio on 03/03/2016.
 */
public class VentanaModel {

    MongoClient mongoClient;
    MongoDatabase mongoDatabase;

    public void conectar(){
        mongoClient = new MongoClient();
        mongoDatabase = mongoClient.getDatabase("DiscoSTU");
    }

    public List<Artistas> getArtistas() throws ParseException {

        FindIterable<Document> findIterable = mongoDatabase.getCollection(Artistas.Coleccion).find();
        return getListaArtistas(findIterable);

    }

    public List<Artistas> getListaArtistas(FindIterable<Document> findIterable) throws ParseException{
        List<Artistas> artistas = new ArrayList<>();
        Artistas artista = null;
        Iterator<Document> iterator = findIterable.iterator();

        while(iterator.hasNext()){
            Document documento = iterator.next();
            artista = new Artistas();
            artista.setId(documento.getObjectId("_id"));
            artista.setNombre(documento.getString("nombre"));
            artista.setTelefono(Integer.parseInt(documento.getString("telefono")));
            artista.setPrecioActuacion(Float.parseFloat(documento.getString("precio de actuacion")));
            artista.setFechaNacimiento(Util.parseFecha(documento.getString("fecha de nacimiento")));
            artistas.add(artista);
        }
        return artistas;
    }

    public List<Canciones> getCanciones() throws ParseException {

        FindIterable<Document> findIterable = mongoDatabase.getCollection(Canciones.Coleccion).find();
        return getListaCanciones(findIterable);

    }

    public List<Canciones> getListaCanciones(FindIterable<Document> findIterable) throws ParseException{
        List<Canciones> canciones = new ArrayList<>();
        Canciones cancion = null;
        Iterator<Document> iterator = findIterable.iterator();

        while(iterator.hasNext()){
            Document documento = iterator.next();
            cancion = new Canciones();
            cancion.setId(documento.getObjectId("_id"));
            cancion.setTitulo(documento.getString("titulo"));
            cancion.setNumeroPista(Integer.parseInt(documento.getString("numero de pista")));
            cancion.setPrecioItunes(Float.parseFloat(documento.getString("precio iTunes")));
            cancion.setFechaSalida(Util.parseFecha(documento.getString("fecha de salida")));
            canciones.add(cancion);
        }
        return canciones;
    }

    public List<Discos> getDiscos() throws ParseException {

        FindIterable<Document> findIterable = mongoDatabase.getCollection(Discos.Coleccion).find();
        return getListaDiscos(findIterable);

    }

    public List<Discos> getListaDiscos(FindIterable<Document> findIterable) throws ParseException{
        List<Discos> discos = new ArrayList<>();
        Discos disco = null;
        Iterator<Document> iterator = findIterable.iterator();

        while(iterator.hasNext()){
            Document documento = iterator.next();
            disco = new Discos();
            disco.setId(documento.getObjectId("_id"));
            disco.setTitulo(documento.getString("titulo"));
            disco.setCopiasVendidas(Integer.parseInt(documento.getString("copias vendidas")));
            disco.setPrecioCompra(Float.parseFloat(documento.getString("precio de compra")));
            disco.setFechaSalida(Util.parseFecha(documento.getString("fecha de salida")));
            discos.add(disco);
        }
        return discos;
    }

    public void anadirArtista(Artistas artista) {

        Document documento = new Document()
                .append("nombre", artista.getNombre())
                .append("telefono", String.valueOf(artista.getTelefono()))
                .append("precio de actuacion", String.valueOf(artista.getPrecioActuacion()))
                .append("fecha de nacimiento", Util.formatFecha(artista.getFechaNacimiento()));
        mongoDatabase.getCollection(Artistas.Coleccion).insertOne(documento);

    }

    public void modificarArtista(Artistas artista) {

        mongoDatabase.getCollection(Artistas.Coleccion).replaceOne(new Document("_id", artista.getId()),
                new Document()
                        .append("nombre", artista.getNombre())
                        .append("telefono", String.valueOf(artista.getTelefono()))
                        .append("precio de actuacion", String.valueOf(artista.getPrecioActuacion()))
                        .append("fecha de nacimiento", Util.formatFecha(artista.getFechaNacimiento())));
    }

    public void anadirCancion(Canciones cancion) {

        Document documento = new Document()
                .append("titulo", cancion.getTitulo())
                .append("numero de pista", String.valueOf(cancion.getNumeroPista()))
                .append("precio iTunes", String.valueOf(cancion.getPrecioItunes()))
                .append("fecha de salida", Util.formatFecha(cancion.getFechaSalida()))
                .append("artista", cancion.getArtista());
        mongoDatabase.getCollection(Canciones.Coleccion).insertOne(documento);

    }

    public void modificarCancion(Canciones cancion) {

        mongoDatabase.getCollection(Canciones.Coleccion).replaceOne(new Document("_id", cancion.getId()),
                new Document()
                        .append("titulo", cancion.getTitulo())
                        .append("numero de pista", String.valueOf(cancion.getNumeroPista()))
                        .append("precio iTunes", String.valueOf(cancion.getPrecioItunes()))
                        .append("fecha de salida", Util.formatFecha(cancion.getFechaSalida()))
                        .append("artista", cancion.getArtista()));
    }

    public void anadirDisco(Discos disco) {

        Document documento = new Document()
                .append("titulo",disco.getTitulo())
                .append("copias vendidas", String.valueOf(disco.getCopiasVendidas()))
                .append("precio de compra", String.valueOf(disco.getPrecioCompra()))
                .append("fecha de salida", Util.formatFecha(disco.getFechaSalida()))
                .append("artista", disco.getArtista());
        mongoDatabase.getCollection(Discos.Coleccion).insertOne(documento);

    }

    public void modificarDisco(Discos disco) {

        mongoDatabase.getCollection(Discos.Coleccion).replaceOne(new Document("_id", disco.getId()),
                new Document()
                        .append("titulo",disco.getTitulo())
                        .append("copias vendidas", String.valueOf(disco.getCopiasVendidas()))
                        .append("precio de compra", String.valueOf(disco.getPrecioCompra()))
                        .append("fecha de salida", Util.formatFecha(disco.getFechaSalida()))
                        .append("artista", disco.getArtista()));
    }

    public void eliminarArtista(String nombre) {
        mongoDatabase.getCollection(Artistas.Coleccion).deleteOne(new Document("nombre", nombre));
    }

    public void eliminarCancion(String titulo) {
        mongoDatabase.getCollection(Canciones.Coleccion).deleteOne(new Document("titulo", titulo));
    }

    public void eliminarDisco(String titulo) {
        mongoDatabase.getCollection(Discos.Coleccion).deleteOne(new Document("titulo", titulo));
    }

    public List<Artistas> buscarArtista(String busqueda) throws ParseException {

        Document documento = new Document("$or", Arrays.asList(
                new Document("nombre", busqueda),
                new Document("telefono", busqueda),
                new Document("precio de actuacion", busqueda)));

        FindIterable findIterable = mongoDatabase.getCollection(Artistas.Coleccion)
                .find(documento)
                .sort(new Document("titulo", 1));
        return getListaArtistas(findIterable);
    }

    public List<Canciones> buscarCancion(String busqueda) throws ParseException {

        Document documento = new Document("$or", Arrays.asList(
                new Document("titulo", busqueda),
                new Document("numero de pista", busqueda),
                new Document("precio iTunes", busqueda),
                new Document("artista", busqueda)));

        FindIterable findIterable = mongoDatabase.getCollection(Canciones.Coleccion)
                .find(documento)
                .sort(new Document("titulo", 1));
        return getListaCanciones(findIterable);
    }

    public List<Discos> buscarDisco(String busqueda) throws ParseException {

        Document documento = new Document("$or", Arrays.asList(
                new Document("titulo", busqueda),
                new Document("copias vendidas", busqueda),
                new Document("precio de compra", busqueda),
                new Document("artista", busqueda)));

        FindIterable findIterable = mongoDatabase.getCollection(Discos.Coleccion)
                .find(documento)
                .sort(new Document("titulo", 1));
        return getListaDiscos(findIterable);
    }


}
