package Gui;

import Base.Artistas;
import Base.Canciones;
import Base.Discos;

import javax.swing.*;
import java.awt.event.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergio on 03/03/2016.
 */
public class VentanaController {

    private VentanaModel model;
    private Ventana view;
    private boolean nuevoArtista;
    private boolean nuevaCancion;
    private boolean nuevoDisco;
    private Artistas artistaSeleccionado;
    private Canciones cancionSeleccionada;
    private Discos discoSeleccionado;

    private enum Tipo {
        Artistas, Canciones, Discos
    }

    public VentanaController(VentanaModel model, Ventana view) {
        this.model = model;
        this.view = view;

        addListeners();
        inicializar();
    }

    public void inicializar(){
        model.conectar();
        listar(Tipo.Artistas);
        listar(Tipo.Canciones);
        listar(Tipo.Discos);
        anadirCombos();
    }

    public void addListeners(){

        view.btNuevoArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevoArtista=true;
                limpiarCajas(Tipo.Artistas, true, nuevoArtista);
                view.btGuardarArtista.setEnabled(true);
            }
        });

        view.btNuevoCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevaCancion=true;
                limpiarCajas(Tipo.Canciones, true, nuevaCancion);
                view.btGuardarCancion.setEnabled(true);
            }
        });

        view.btNuevoDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevoDisco=true;
                limpiarCajas(Tipo.Discos, true, nuevoDisco);
                view.btGuardarDisco.setEnabled(true);
            }
        });

        view.btEditarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevoArtista=false;
                limpiarCajas(Tipo.Artistas, true, nuevoArtista);
                view.btGuardarArtista.setEnabled(true);
                view.btNuevoArtista.setEnabled(false);
                view.btEliminarArtista.setEnabled(false);
            }
        });

        view.btEditarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevaCancion=false;
                limpiarCajas(Tipo.Canciones, true, nuevaCancion);
                view.btGuardarCancion.setEnabled(true);
                view.btNuevoCancion.setEnabled(false);
                view.btEliminarCancion.setEnabled(false);
            }
        });

        view.btEditarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                nuevoDisco=false;
                limpiarCajas(Tipo.Discos, true, nuevoDisco);
                view.btGuardarDisco.setEnabled(true);
                view.btNuevoDisco.setEnabled(false);
                view.btEliminarDisco.setEnabled(false);
            }
        });

        view.btGuardarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                guardar(Tipo.Artistas, nuevoArtista);
                view.btNuevoArtista.setEnabled(true);
                view.btGuardarArtista.setEnabled(false);
                view.btEditarArtista.setEnabled(false);
                view.btEliminarArtista.setEnabled(false);
            }
        });

        view.btGuardarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                guardar(Tipo.Canciones, nuevaCancion);
                view.btNuevoCancion.setEnabled(true);
                view.btGuardarCancion.setEnabled(false);
                view.btEditarCancion.setEnabled(false);
                view.btEliminarCancion.setEnabled(false);
            }
        });

        view.btGuardarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                guardar(Tipo.Discos, nuevoDisco);
                view.btNuevoDisco.setEnabled(true);
                view.btGuardarDisco.setEnabled(false);
                view.btEditarDisco.setEnabled(false);
                view.btEliminarDisco.setEnabled(false);
            }
        });

        view.btEliminarArtista.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                eliminar(Tipo.Artistas);
                limpiarCajas(Tipo.Artistas, false, true);
                view.btNuevoArtista.setEnabled(true);
                view.btGuardarArtista.setEnabled(false);
                view.btEditarArtista.setEnabled(false);
                view.btEliminarArtista.setEnabled(false);
            }
        });

        view.btEliminarCancion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                eliminar(Tipo.Canciones);
                limpiarCajas(Tipo.Canciones, false, true);
                view.btNuevoCancion.setEnabled(true);
                view.btGuardarCancion.setEnabled(false);
                view.btEditarCancion.setEnabled(false);
                view.btEliminarCancion.setEnabled(false);
            }
        });

        view.btEliminarDisco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                eliminar(Tipo.Discos);
                limpiarCajas(Tipo.Discos, false, true);
                view.btNuevoDisco.setEnabled(true);
                view.btGuardarDisco.setEnabled(false);
                view.btEditarDisco.setEnabled(false);
                view.btEliminarDisco.setEnabled(false);
            }
        });

        view.listArtistas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                view.btEditarArtista.setEnabled(true);
                view.btEliminarArtista.setEnabled(true);
                mostrarDatos(Tipo.Artistas);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        view.listCanciones.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                view.btEditarCancion.setEnabled(true);
                view.btEliminarCancion.setEnabled(true);
                mostrarDatos(Tipo.Canciones);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        view.listDiscos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                view.btEditarDisco.setEnabled(true);
                view.btEliminarDisco.setEnabled(true);
                mostrarDatos(Tipo.Discos);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });

        view.tfBuscarArtista.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (view.tfBuscarArtista.getText().equals("")) {
                    listar(Tipo.Artistas);
                    return;
                }

                List<Artistas> listaArtistas = new ArrayList<>();
                try {
                    listaArtistas = model.buscarArtista(view.tfBuscarArtista.getText());
                } catch (ParseException pe) {
                    Util.Util.mensajeError("Algún dato no se puede cargar", "Error al cargar");
                }
                view.modelArtistas.removeAllElements();
                for (Artistas artista : listaArtistas){
                    view.modelArtistas.addElement(artista);
                }
            }
        });

        view.tfBuscarCancion.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (view.tfBuscarCancion.getText().equals("")) {
                    listar(Tipo.Canciones);
                    return;
                }

                List<Canciones> listaCanciones = new ArrayList<>();
                try {
                    listaCanciones = model.buscarCancion(view.tfBuscarCancion.getText());
                } catch (ParseException pe) {
                    Util.Util.mensajeError("Algún dato no se puede cargar", "Error al cargar");
                }
                view.modelCanciones.removeAllElements();
                for (Canciones cancion : listaCanciones){
                    view.modelCanciones.addElement(cancion);
                }
            }
        });

        view.tfBuscarDisco.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (view.tfBuscarDisco.getText().equals("")) {
                    listar(Tipo.Discos);
                    return;
                }

                List<Discos> listaDiscos = new ArrayList<>();
                try {
                    listaDiscos = model.buscarDisco(view.tfBuscarDisco.getText());
                } catch (ParseException pe) {
                    Util.Util.mensajeError("Algún dato no se puede cargar", "Error al cargar");
                }
                view.modelDiscos.removeAllElements();
                for (Discos disco : listaDiscos) {
                    view.modelDiscos.addElement(disco);
                }
            }
        });

    }

    public void limpiarCajas(Tipo tipo, boolean editable, boolean nuevo){

        switch(tipo){

            case Artistas:
                view.tfNombreArtista.setEnabled(editable);
                view.tfNombreArtista.setEditable(editable);
                view.tfTelefonoArtista.setEnabled(editable);
                view.tfTelefonoArtista.setEditable(editable);
                view.tfPrecioArtista.setEnabled(editable);
                view.tfPrecioArtista.setEditable(editable);
                view.calFechaNacimientoArtista.setEnabled(editable);

                if(nuevo){
                    view.tfNombreArtista.setText("");
                    view.tfTelefonoArtista.setText("");
                    view.tfPrecioArtista.setText("");
                    view.calFechaNacimientoArtista.setDate(null);
                }
                break;

            case Canciones:
                view.tfTituloCancion.setEnabled(editable);
                view.tfTituloCancion.setEditable(editable);
                view.tfPrecioItunesCancion.setEnabled(editable);
                view.tfPrecioItunesCancion.setEditable(editable);
                view.tfNumeroPistaCancion.setEnabled(editable);
                view.tfNumeroPistaCancion.setEditable(editable);
                view.calFechaSalidaCancion.setEnabled(editable);
                view.cbArtistaCancion.setEnabled(editable);
                view.cbArtistaCancion.setEditable(editable);

                if(nuevo){
                    view.tfTituloCancion.setText("");
                    view.tfPrecioItunesCancion.setText("");
                    view.tfNumeroPistaCancion.setText("");
                    view.calFechaSalidaCancion.setDate(null);
                }
                break;

            case Discos:
                view.tfTituloDisco.setEnabled(editable);
                view.tfTituloDisco.setEditable(editable);
                view.tfPrecioCompraDisco.setEnabled(editable);
                view.tfPrecioCompraDisco.setEditable(editable);
                view.tfCopiasVendidasDisco.setEnabled(editable);
                view.tfCopiasVendidasDisco.setEditable(editable);
                view.calFechaSalidaDisco.setEnabled(editable);
                view.cbArtistaDisco.setEnabled(editable);
                view.cbArtistaDisco.setEditable(editable);

                if(nuevo){
                    view.tfTituloDisco.setText("");
                    view.tfPrecioCompraDisco.setText("");
                    view.tfCopiasVendidasDisco.setText("");
                    view.calFechaSalidaDisco.setDate(null);
                }
                break;

        }

    }

    public void listar(Tipo tipo){

        switch(tipo){

            case Artistas:
                try{
                    List<Artistas> artistas = model.getArtistas();

                    view.modelArtistas.removeAllElements();

                    for(Artistas artista: artistas){
                        view.modelArtistas.addElement(artista);
                    }
                } catch(ParseException pe){
                    pe.printStackTrace();
                }
                break;

            case Canciones:
                try{
                    List<Canciones> canciones = model.getCanciones();

                    view.modelCanciones.removeAllElements();

                    for(Canciones cancion: canciones){
                        view.modelCanciones.addElement(cancion);
                    }
                } catch(ParseException pe){
                    pe.printStackTrace();
                }
                break;

            case Discos:
                try{
                    List<Discos> discos = model.getDiscos();

                    view.modelDiscos.removeAllElements();

                    for(Discos disco: discos){
                        view.modelDiscos.addElement(disco);
                    }
                } catch(ParseException pe){
                    pe.printStackTrace();
                }
                break;
        }

    }

    public void mostrarDatos(Tipo tipo){

        switch(tipo){

            case Artistas:
                artistaSeleccionado = (Artistas)view.listArtistas.getSelectedValue();
                view.tfNombreArtista.setText(artistaSeleccionado.getNombre());
                view.tfPrecioArtista.setText(String.valueOf(artistaSeleccionado.getPrecioActuacion()));
                view.tfTelefonoArtista.setText(String.valueOf(artistaSeleccionado.getTelefono()));
                view.calFechaNacimientoArtista.setDate(artistaSeleccionado.getFechaNacimiento());
                break;

            case Canciones:
                cancionSeleccionada = (Canciones)view.listCanciones.getSelectedValue();
                view.tfTituloCancion.setText(cancionSeleccionada.getTitulo());
                view.tfPrecioItunesCancion.setText(String.valueOf(cancionSeleccionada.getPrecioItunes()));
                view.tfNumeroPistaCancion.setText(String.valueOf(cancionSeleccionada.getNumeroPista()));
                view.calFechaSalidaCancion.setDate(cancionSeleccionada.getFechaSalida());
                break;

            case Discos:
                discoSeleccionado = (Discos)view.listDiscos.getSelectedValue();
                view.tfTituloDisco.setText(discoSeleccionado.getTitulo());
                view.tfPrecioCompraDisco.setText(String.valueOf(discoSeleccionado.getPrecioCompra()));
                view.tfCopiasVendidasDisco.setText(String.valueOf(discoSeleccionado.getCopiasVendidas()));
                view.calFechaSalidaDisco.setDate(discoSeleccionado.getFechaSalida());
                break;
        }

    }

    public void guardar(Tipo tipo, boolean nuevo){

        switch(tipo){

            case Artistas:
                Artistas artista;
                if (nuevo){
                    artista = new Artistas();
                } else{
                    artista = artistaSeleccionado;
                }

                artista.setNombre(view.tfNombreArtista.getText());
                artista.setPrecioActuacion(Float.parseFloat(view.tfPrecioArtista.getText()));
                artista.setTelefono(Integer.parseInt(view.tfTelefonoArtista.getText()));
                artista.setFechaNacimiento(view.calFechaNacimientoArtista.getDate());

                if (nuevo){
                    model.anadirArtista(artista);
                } else{
                    model.modificarArtista(artista);
                }
                limpiarCajas(Tipo.Artistas, false, true);
                listar(Tipo.Artistas);
                anadirCombos();
                break;

            case Canciones:
                Canciones cancion;
                if(nuevo){
                    cancion = new Canciones();
                } else {
                    cancion = cancionSeleccionada;
                }

                cancion.setTitulo(view.tfTituloCancion.getText());
                cancion.setPrecioItunes(Float.parseFloat(view.tfPrecioItunesCancion.getText()));
                cancion.setNumeroPista(Integer.parseInt(view.tfNumeroPistaCancion.getText()));
                cancion.setFechaSalida(view.calFechaSalidaCancion.getDate());
                cancion.setArtista(view.cbArtistaCancion.getSelectedItem().toString());

                if (nuevo) {
                    model.anadirCancion(cancion);
                } else {
                    model.modificarCancion(cancion);
                }
                limpiarCajas(Tipo.Canciones, false, true);
                listar(Tipo.Canciones);
                break;

            case Discos:
                Discos disco;
                if(nuevo){
                    disco = new Discos();
                } else {
                    disco = discoSeleccionado;
                }

                disco.setTitulo(view.tfTituloDisco.getText());
                disco.setPrecioCompra(Float.parseFloat(view.tfPrecioCompraDisco.getText()));
                disco.setCopiasVendidas(Integer.parseInt(view.tfCopiasVendidasDisco.getText()));
                disco.setFechaSalida(view.calFechaSalidaDisco.getDate());
                disco.setArtista(view.cbArtistaDisco.getSelectedItem().toString());

                if(nuevo){
                    model.anadirDisco(disco);
                } else {
                    model.modificarDisco(disco);
                }
                limpiarCajas(Tipo.Discos, false, true);
                listar(Tipo.Discos);
                break;

        }
    }

    public void eliminar(Tipo tipo){

        switch(tipo){

            case Artistas:
                if ((Util.Util.mensajeConfirmacion("¿Está seguro?", "Eliminar artista")) == JOptionPane.NO_OPTION)
                    return;
                artistaSeleccionado = (Artistas)view.listArtistas.getSelectedValue();
                model.eliminarArtista(artistaSeleccionado.getNombre());
                listar(Tipo.Artistas);
                anadirCombos();
                break;

            case Canciones:
                if ((Util.Util.mensajeConfirmacion("¿Está seguro?", "Eliminar canción")) == JOptionPane.NO_OPTION)
                    return;
                cancionSeleccionada = (Canciones)view.listCanciones.getSelectedValue();
                model.eliminarCancion(cancionSeleccionada.getTitulo());
                listar(Tipo.Canciones);
                break;

            case Discos:
                if ((Util.Util.mensajeConfirmacion("¿Está seguro?", "Eliminar disco")) == JOptionPane.NO_OPTION)
                    return;
                discoSeleccionado = (Discos)view.listDiscos.getSelectedValue();
                model.eliminarDisco(discoSeleccionado.getTitulo());
                listar(Tipo.Discos);
                break;

        }

    }

    public void anadirCombos(){
        try{
            List<Artistas> artistas = model.getArtistas();
            view.cbArtistaCancion.removeAllItems();
            view.cbArtistaDisco.removeAllItems();
            for(Artistas artista: artistas){
                view.cbArtistaCancion.addItem(artista);
                view.cbArtistaDisco.addItem(artista);
            }
        } catch(ParseException pe){
            pe.printStackTrace();
        }
    }

}
