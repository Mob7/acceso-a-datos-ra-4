package Gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by sergio on 03/03/2016.
 */
public class Ventana {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JTextField tfTituloDisco;
    public JTextField tfPrecioCompraDisco;
    public JTextField tfCopiasVendidasDisco;
    public JComboBox cbArtistaDisco;
    public JDateChooser calFechaSalidaDisco;
    public JTextField tfTituloCancion;
    public JTextField tfPrecioItunesCancion;
    public JTextField tfNumeroPistaCancion;
    public JDateChooser calFechaSalidaCancion;
    public JComboBox cbArtistaCancion;
    public JTextField tfNombreArtista;
    public JTextField tfTelefonoArtista;
    public JTextField tfPrecioArtista;
    public JDateChooser calFechaNacimientoArtista;
    public JButton btNuevoArtista;
    public JButton btGuardarArtista;
    public JButton btEditarArtista;
    public JButton btEliminarArtista;
    public JButton btNuevoCancion;
    public JButton btGuardarCancion;
    public JButton btEditarCancion;
    public JButton btEliminarCancion;
    public JButton btNuevoDisco;
    public JButton btGuardarDisco;
    public JButton btEditarDisco;
    public JButton btEliminarDisco;
    public JList listDiscos;
    public JList listCanciones;
    public JList listArtistas;
    public JTextField tfBuscarArtista;
    public JTextField tfBuscarCancion;
    public JTextField tfBuscarDisco;
    public DefaultListModel modelArtistas;
    public DefaultListModel modelCanciones;
    public DefaultListModel modelDiscos;

    public Ventana() {
        JFrame frame = new JFrame("Música");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        inicializar();
    }

    public void inicializar(){

        modelArtistas = new DefaultListModel();
        listArtistas.setModel(modelArtistas);
        modelCanciones = new DefaultListModel();
        listCanciones.setModel(modelCanciones);
        modelDiscos = new DefaultListModel();
        listDiscos.setModel(modelDiscos);

    }
}
