import Gui.Ventana;
import Gui.VentanaController;
import Gui.VentanaModel;

/**
 * Created by sergio on 03/03/2016.
 */
public class Principal {

    public static void main(String args[]) {
        Ventana ventana = new Ventana();
        VentanaModel model = new VentanaModel();
        VentanaController controller = new VentanaController(model, ventana);
    }
}
